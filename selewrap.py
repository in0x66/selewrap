#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re


class BaseObject(object):

    class ImproperClassNameException(Exception):
        pass

    class WrongTypeException(Exception):
        pass

    @classmethod
    def is_classname_correct(self):
        return re.match(r"^[A-Z].*?$", self.__name__) is not None

    def __new__(self, *args, **kwargs):
        if not self.is_classname_correct():
            raise self.ImproperClassNameException(
                "%s is not an appropriate class name (must start with [A-Z]" % self.__name__
            )
        return object.__new__(self, *args, **kwargs)


class BaseDecorator(BaseObject):
    def __init__(self):
        raise NotImplementedError

    def __call__(self):
        raise NotImplementedError


class Accepts(BaseDecorator):

    class BadNumberOfArgumentsException(Exception):
        pass

    class BadTypeOfArgument(Exception):
        pass

    def __init__(self, *args):
        self.args = args

    def __call__(self, function):
        def wrapped(*args):
            if len(self.args) != len(args):
                raise self.BadNumberOfArgumentsException(
                    "Bad number of arguments specified! (expected %d, got %d)" % (len(self.args),
                                                                                  len(args))
                )
            for counter, argument in enumerate(zip(self.args, args)):
                first, second = argument
                if issubclass(type(second), first) and first is not None:
                    raise self.BadTypeOfArgument(
                        "Bad type of argument specified on position %d (expected %s, got %s)" %
                        (counter + 1, str(first.__name__), str(type(second).__name__))
                    )
            return function(*args)
        wrapped.__name__ = function.__name__
        return wrapped


class Construct(BaseDecorator):

    class BadNumberOfArgumentsException(Exception):
        pass

    class BadTypeOfArgument(Exception):
        pass

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __call__(self, obj):
        obj._construct_args = self.args
        obj._construct_kwargs = self.kwargs
        return obj


class Selenium(BaseObject):

    def __init__(self, driver, default_timeout=5):
        self.driver = driver
        self.timeout = default_timeout

    def __getattribute__(self, name):
        try:
            return super(Selenium, self).__getattribute__(name)
        except AttributeError as e:
            try:
                return self.driver.__getattribute__(name)
            except AttributeError:
                raise e

    def find_element(self, element, timeout=None):
        return WebDriverWait(self.driver, timeout or self.timeout)\
            .until(EC.presence_of_element_located(element.locator))

    def find_element_wait_clickable(self, element, timeout=None):
        return WebDriverWait(self.driver, timeout or self.timeout)\
            .until(EC.element_to_be_clickable(element.locator))


class FinalElement(BaseObject):
    pass


class Element(BaseObject):

    class UnboundElementException(Exception):
        pass

    def __init__(self, location, by=By.XPATH, transition=None):
        self.location = location
        self.by = by
        self.transition = transition
        if self.transition:
            if not issubclass(self.transition, BasePage):
                raise self.WrongTypeException()

        if not isinstance(self, FinalElement):
            for name, element in self.__class__.__dict__.iteritems():
                obj = element
                if type(element) is type and issubclass(element, Element):
                    # Instantiate
                    try:
                        assert "_construct_args" in element.__dict__
                        assert "_construct_kwargs" in element.__dict__
                    except AssertionError:
                        raise Exception("You must specify a @Construct decorator")
                    obj = element(*getattr(element, "_construct_args"),
                                  **getattr(element, "_construct_kwargs"))
                    self.__dict__[name] = obj.bind(self)
                    if name in self.__dict__:
                        raise Exception("You cannot use name '%s' as an object name, it would override" % name)
                elif isinstance(obj, Element):
                    self.__dict__[name] = obj.bind(self)
                    if name in self.__dict__:
                        raise Exception("You cannot use name '%s' as an object name, it would override" % name)

    @property
    def is_bound(self):
        try:
            self.parent_element
            return self.parent_element is not None
        except AttributeError:
            return False

    @property
    def is_area(self):
        return isinstance(self, Area)

    @property
    def is_final(self):
        return isinstance(self, FinalElement)

    def bind(self, element):
        assert self != element, "This would cause recursion"
        self.parent_element = element
        return self

    @property
    def driver(self):
        try:
            self.parent_element
        except AttributeError:
            raise self.UnboundElementException("Element %s is not bound!" % self.__class__.__name__)
        try:
            return self.parent_element.driver
        except AttributeError:
            raise self.UnboundElementException("Element %s is not bound!" % self.parent_element.__class__.__name__)

    @property
    def locator(self):
        return self.by, self.location

    @property
    def selenium_element(self):
        # Predelat na iterativni ziskavani elementu, tak bude mozno dosahnout zanorovani
        return self.driver.find_element(self)

    def get_attribute(self, attribute):
        return self.selenium_element.get_attribute(attribute)

    @property
    def id(self):
        return self.get_attribute("id")

    @property
    def tag(self):
        return self.selenium_element.tag_name

    @property
    def text(self):
        return self.selenium_element.text

    @property
    def klass(self):
        return self.get_attribute("class")

    def click(self, timeout=None):
        self.driver.find_element_wait_clickable(self, timeout=timeout).click()
        if self.transition:
            return self.transition(self.driver)
        else:
            return None

    @property
    def child_elements(self):
        for name, variable in self.__dict__.iteritems():
            if isinstance(variable, Element):
                yield variable


class InputElement(Element, FinalElement):
    @property
    def value(self):
        return self.get_attribute("value")

    @property
    def name(self):
        return self.get_attribute("name")

    @property
    def type(self):
        return self.get_attribute("type")


class InputTextElement(InputElement):
    def write(self, text):
        return self.selenium_element.send_keys(text)


class InputCheckboxElement(InputElement):

    @property
    def checked(self):
        return self.selenium_element.is_selected()

    def check(self):
        if not self.checked:
            return self.click()
        else:
            return None

    def uncheck(self):
        if self.checked:
            return self.click()
        else:
            return None

    def set_bool(self, value):
        if value:
            return self.check()
        else:
            return self.uncheck()

    def set_string(self, value):
        value = value.lower().strip()
        if value in ["ok", "on", "yes", "checked", "selected", "enabled", "true"]:
            return self.check()
        if value in ["unselected", "unchecked", "off", "disabled", "false"]:
            return self.uncheck()
        try:
            return self.set_int(int(value))
        except ValueError:
            pass
        # Fuck you all
        return self.set_bool(bool(value))

    def set_int(self, value):
        return self.set_bool(value != 0)

    def set(self, value):
        if not isinstance(value, bool):
            if isinstance(value, str):
                return self.set_string(value)
            elif isinstance(value, int):
                return self.set_int(value)
            else:
                try:
                    self.set_bool(bool(value))
                except ValueError:
                    raise Exception("Whole world is dooomed!")
        return self.set_bool(value)


class FormButton(InputElement):
    pass


class Area(Element):
    pass


class Form(Area):

    def fill(self, data):
        for element in self.child_elements:
            if not isinstance(element, InputElement):
                continue
            if element.name not in data:
                continue
            value = data[element.name]
            # input type text,password,textarea
            if isinstance(element, InputTextElement):
                element.write(value)
                del data[element.name]
            # input type checkbox
            elif isinstance(element, InputCheckboxElement):
                element.set(value)
                del data[element.name]
        if len(data) > 0:
            raise Exception("fail")
        return self

    def submit(self, timeout=None):
        for element in self.child_elements:
            if not isinstance(element, FormButton):
                continue
            if element.type == "submit":
                return element.click(timeout=timeout)
        raise Exception("No submit button")

    def fill_submit(self, data, timeout=None):
        return self.fill(data).submit(timeout=timeout)


class BasePage(BaseObject):

    def __init__(self, driver):
        self.driver = driver

        for name, area in self.__class__.__dict__.iteritems():
            obj = area
            if type(area) is type and issubclass(area, Area):
                # Instantiate
                try:
                    assert "_construct_args" in area.__dict__
                    assert "_construct_kwargs" in area.__dict__
                except AssertionError:
                    raise Exception("You must specify a @Construct decorator")
                obj = area(*getattr(area, "_construct_args"), **getattr(area, "_construct_kwargs"))
                self.__dict__[name] = obj.bind(self)
            elif isinstance(obj, Area):
                if name in self.__dict__:
                    raise Exception("You cannot use name '%s' as an object name, it would override" % name)
                self.__dict__[name] = obj.bind(self)


class SomePage(BasePage):

    @Construct("#search", by=By.CSS_SELECTOR)
    class Search(Area):

        @Construct("#inet-f", by=By.CSS_SELECTOR)
        class SearchForm(Form):
            aksdbaidugsadf = InputTextElement("//[@name='q']")
            dsaijfbgisdfg = FormButton("#hledej", by=By.CSS_SELECTOR)

    def search_something(self, term):
        self.Search.SearchForm.fill(dict(q=term))
        return self.Search.SearchForm.submit()


if __name__ == "__main__":
    from selenium import webdriver
    ff = Selenium(webdriver.Firefox())
    ff.get("http://seznam.cz")
    pg = SomePage(ff)
    pg.search_something("windows sux")
    import time
    time.sleep(10)
    ff.close()
